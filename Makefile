.PHONY:default
default:
	$(error Must specify target. One of: clean)

ifeq ($(OS),Windows_NT)
RM:=cmd /c rd /s /q
else
RM:=rm -Rf
endif

.PHONY:clean
clean:
	-$(RM) "./timer_addon/build"
	-$(RM) "./timer_addon/node_modules"
	-$(RM) "./timer_app/node_modules"
