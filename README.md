# Electron app with native addon (using CMake) in the same repo

npm link: https://docs.npmjs.com/cli/link

0. Install cmake - https://cmake.org/

1. Clone this repo

2. `cd timer_app`

3. `npm install`

4. `./node_modules/.bin/electron .` (OS X, Linux); `node_modules\.bin\electron .` (Windows)

If you get some numbers printed out on the console (native addon raising events that are handled in the Electron main process) and an incrementing number in the HTML page (native addon raising events that are handled in the Electron render process) then it worked. Don't be afraid to quit once it's counted up to 10 or so... you've seen it all.
