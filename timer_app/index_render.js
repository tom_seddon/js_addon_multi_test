const $ = require('jquery');
const remote = require('electron').remote;
const Message = remote.require('./Message.js');
const Timer = remote.require('timer_addon');

console.log("hello\n");

let tick = 0;
const timer = new Timer();
timer.on('tick', () => {
    $("#counter").text(tick);
    //console.log('tick: ' + tick);
    ++tick;
});
timer.start(100, 100);
Message('hello from renderer process');

window.onbeforeunload = () => {
    timer.stop();
};
