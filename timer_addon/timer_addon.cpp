#include <node.h>
#include <nan.h>
#ifdef USE_CPP_LIB
#include <cpp_lib.h>
#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

class Timer :public Nan::ObjectWrap {
public:
	static void Init(v8::Local<v8::Object> exports);
protected:
private:
	Timer() = default;
	~Timer();

	static void New(const Nan::FunctionCallbackInfo<v8::Value> &info);
	static void Start(const Nan::FunctionCallbackInfo<v8::Value> &info);
	static void Stop(const Nan::FunctionCallbackInfo<v8::Value> &info);
	static Nan::Persistent<v8::Function> ms_constructor;
	static void OnTick(uv_timer_t *handle);
	uv_timer_t m_timer;
	bool m_timer_started = false;
	Nan::Callback *m_emit = nullptr;

	void StopTimer();
};

//////////////////////////////////////////////////////////////////////////
////////////////////////y//////////////////////////////////////////////////

Nan::Persistent<v8::Function> Timer::ms_constructor;

Timer::~Timer() {
	this->StopTimer();

	delete m_emit;
	m_emit = nullptr;
}

void Timer::Init(v8::Local<v8::Object> exports) {
	Nan::HandleScope scope;

	v8::Local<v8::FunctionTemplate> tp = Nan::New<v8::FunctionTemplate>(&New);
	tp->SetClassName(Nan::New("Timer").ToLocalChecked());

	// (the internal field slot is used by ObjectWrap::Wrap)
	tp->InstanceTemplate()->SetInternalFieldCount(1);

	Nan::SetPrototypeMethod(tp, "start", &Start);
	Nan::SetPrototypeMethod(tp, "stop", &Stop);

	ms_constructor.Reset(tp->GetFunction());
	exports->Set(Nan::New("Timer").ToLocalChecked(), tp->GetFunction());
}

void Timer::New(const Nan::FunctionCallbackInfo<v8::Value> &info) {
	if (!info.IsConstructCall()) {
		Nan::ThrowError("`new' required");
		return;
	}

	v8::Isolate *isolate = info.GetIsolate();
	v8::Local<v8::Context> context = isolate->GetCurrentContext();

	auto self = new Timer;
	self->Wrap(info.This());
	v8::Local<v8::Object> self_handle = self->handle();

	Nan::MaybeLocal<v8::Value> maybe_emit = self_handle->Get(context, Nan::New("emit").ToLocalChecked());
	if (maybe_emit.IsEmpty()) {
		Nan::ThrowError("`emit' is undefined");
		return;
	}

	v8::Local<v8::Value> emit_value = maybe_emit.ToLocalChecked();

	if (!emit_value->IsFunction()) {
		Nan::ThrowError("`emit' is not a function");
		return;
	}

	self->m_emit = new Nan::Callback(v8::Local<v8::Function>::Cast(emit_value));
	info.GetReturnValue().Set(info.This());
}

void Timer::Start(const Nan::FunctionCallbackInfo<v8::Value> &info) {
	auto self = Nan::ObjectWrap::Unwrap<Timer>(info.Holder());

	if (self->m_timer_started) {
		return;
	}

	uint64_t timeout = info[0]->IsUndefined() ? 0 : (uint64_t)info[0]->NumberValue();
	uint64_t repeat = info[1]->IsUndefined() ? 0 : (uint64_t)info[1]->NumberValue();

	uv_timer_init(uv_default_loop(), &self->m_timer);
	uv_timer_start(&self->m_timer, &OnTick, timeout, repeat);
	self->m_timer_started = true;
	self->m_timer.data = self;
}

void Timer::Stop(const Nan::FunctionCallbackInfo<v8::Value> &info) {
	auto self = Nan::ObjectWrap::Unwrap<Timer>(info.Holder());

	self->StopTimer();
}

void Timer::StopTimer() {
	if (m_timer_started) {
		uv_timer_stop(&m_timer);
		m_timer_started = false;
	}
}

void Timer::OnTick(uv_timer_t *handle) {
	Nan::HandleScope scope;

	auto self = (Timer *)handle->data;

	v8::Local<v8::Value> argv[] = { Nan::New("tick").ToLocalChecked() };
	int argc = sizeof argv / sizeof argv[0];
	self->m_emit->Call(self->handle(), argc, argv);
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

static void GetVersion(const v8::FunctionCallbackInfo<v8::Value> &args) {
	v8::Isolate *isolate = args.GetIsolate();

	args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, "blahblah"));
}

#ifdef USE_CPP_LIB
static void GetSumWrap(const v8::FunctionCallbackInfo<v8::Value> &args) {
	v8::Isolate *isolate = args.GetIsolate();
	v8::Local<v8::Context> context = isolate->GetCurrentContext();

	v8::Maybe<double> a_ = args[0]->NumberValue(context);
	if (a_.IsNothing()) {
		Nan::ThrowError("`a' is not a number");
		return;
	}

	v8::Maybe<double> b_ = args[1]->NumberValue(context);
	if (b_.IsNothing()) {
		Nan::ThrowError("`b' is not a number");
		return;
	}

	double a = a_.FromJust();
	double b = b_.FromJust();
	double result = GetSum(a, b);

	args.GetReturnValue().Set(v8::Number::New(isolate, result));
}
#endif

#ifdef USE_CPP_LIB
static void GetStringWrap(const v8::FunctionCallbackInfo<v8::Value> &args) {
	v8::Isolate *isolate = args.GetIsolate();
	args.GetReturnValue().Set(v8::String::NewFromUtf8(isolate, GetString()));
}
#endif

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

void Init(v8::Local<v8::Object> exports) {
	NODE_SET_METHOD(exports, "getVersion", &GetVersion);
	Timer::Init(exports);
#ifdef USE_CPP_LIB
	NODE_SET_METHOD(exports, "getSum", &GetSumWrap);
	NODE_SET_METHOD(exports, "getString", &GetStringWrap);
#endif
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

NODE_MODULE(addon, Init)
