let hello
try {
    hello = require('./build/Debug/timer_addon');
} catch (e) {
    hello = require('./build/Release/timer_addon');
}

console.log("Timer.js: module keys: " + Object.keys(hello));
console.log("Timer.js: module version: " + hello.getVersion());
//console.log("timer: " + hello.Timer);

const EventEmitter = require('events').EventEmitter;
const inherits = require('util').inherits;
inherits(hello.Timer, EventEmitter);

module.exports = hello.Timer;
