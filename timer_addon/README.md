# Any

You'll need node-gyp.

    npm install -g node-gyp

Do the usual npm thing.

    npm install

# OS X/Linux

Configure with node-gyp:

    node-gyp configure --target=$(./node_modules/.bin/electron --version) --arch=x64 --dist-url=https://atom.io/download/electron --devdir=$HOME/.electron-gyp/.node-gyp

Make the build folder:

	make -C build
	
Run the program:

	./node_modules/.bin/electron .

# Windows

Get Electron version:

    .\node_modules\.bin\electron.cmd --version

When calling =node-gyp=, pass the version in as the argument to
`--target`. (For me, I happened to get Electron version 1.6.11.)

    node-gyp rebuild --target=1.6.11 --arch=x64 --dist-url=https://atom.io/download/electron

** VS2017 Debugging

Install [Microsoft Child Process Debugging Power Tool](https://marketplace.visualstudio.com/items?itemName%3DGreggMiskelly.MicrosoftChildProcessDebuggingPowerTool).

Activate it with `Debug` > `Other Debug Targets` > `Child Process
Debugging Settings...`.

[Electron has a symbol server](https://electron.atom.io/docs/development/setting-up-symbol-server/).

Set debugging properties:

Command: `C:\Windows\System32\cmd.exe`
Command Arguments: `/c node_modules\.bin\electron.cmd .`
Working Directory: `$(ProjectDir)..\`
